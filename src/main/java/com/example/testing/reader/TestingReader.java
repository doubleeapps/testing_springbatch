package com.example.testing.reader;

import com.example.testing.domain.Testing;
import com.example.testing.domain.TestingRowMapper;
import com.example.testing.util.MappingPagingProviderQuery;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.AbstractSqlPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@StepScope
public class TestingReader extends JdbcPagingItemReader<Testing> {
    @Autowired
    @Qualifier("dwhDataSource")
    private DataSource dwhDataSource;

    @Value("${clicks.default.fetchsize}")
    private int defaultFetchSize;

    @Value("${clicks.fetchSize.cache.name}")
    private String fetchSizeKey;

    @Value("#{stepExecution}")
    private StepExecution stepExecution;

    @PostConstruct
    public void init() {
        setDataSource(dwhDataSource);
        setFetchSize(defaultFetchSize);
        setPageSize(defaultFetchSize);
        setRowMapper(new TestingRowMapper());
        setQueryProvider(createQueryProvider(dwhDataSource));
    }

    public PagingQueryProvider createQueryProvider(DataSource dataSource) {
//        Date date = Utils.subtractDays(stepExecution.getJobExecution().getJobParameters().getDate("date"), 1);
        AbstractSqlPagingQueryProvider queryProvider = new MappingPagingProviderQuery().determineQueryProvider(dataSource);
        queryProvider.setSelectClause("SELECT BUSINESS_DATE, ID, CBDATE, CBCIFNO, CBACTN, CBSCCD, CBCTYP, CBSCDS, CBSCTC, CBSCTM, CBLBAL, CBLBALE, CBBKCD, CBFXRT");
        queryProvider.setFromClause("Testing");
        queryProvider.setSortKeys(sortByIDNoAsc());
//        queryProvider.setWhereClause("CONVERT(varchar(10), BUSINESS_DATE, 120) = '"+Utils.getDateFormatFromDate("yyyy-MM-dd", date)+"'");
        return queryProvider;
    }

    private Map<String, Order> sortByIDNoAsc(){
        Map<String, Order> sortConfiguration = new HashMap<>();
        sortConfiguration.put("ID", Order.ASCENDING);
        return sortConfiguration;
    }
}
