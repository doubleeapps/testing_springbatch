package com.example.testing.writer;

import com.example.testing.domain.TestingDataWriter;
import com.example.testing.repository.NewDBRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@StepScope
public class WriterTesting implements ItemWriter<TestingDataWriter> {

    @Autowired
    NewDBRepository newDBRepository;

    @Override
    public void write(List<? extends TestingDataWriter> testingDataWriters) {
        newDBRepository.executeInsertBatch(testingDataWriters);
    }
}
