package com.example.testing.processor;

import com.example.testing.domain.NewDB;
import com.example.testing.domain.Testing;
import com.example.testing.domain.TestingDataWriter;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

@Component
@StepScope
public class TestingProcessor implements ItemProcessor<Testing, TestingDataWriter> {
    @Override
    public TestingDataWriter process(Testing testing) throws Exception {
        TestingDataWriter testingDataWriter = new TestingDataWriter();
        NewDB newDB = new NewDB();
        newDB.setId((long)generateRandom());
        newDB.setCategoryId(testing.getCbdate());
        newDB.setYearMonth("1112");
        newDB.setCif("12");
        newDB.setProcessingDate(new Date());
        newDB.setTotalAmountIdr(BigDecimal.valueOf(100));
        testingDataWriter.setNewDB(newDB);
        return testingDataWriter;
    }

    private double generateRandom(){
        return Math.random() * 999999999 + 1;
    }
}
