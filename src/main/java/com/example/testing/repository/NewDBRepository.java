package com.example.testing.repository;

import com.example.testing.domain.NewDB;
import com.example.testing.domain.TestingDataWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class NewDBRepository {
    @Value("${testing.table.name}")
    private String tableName;

    @Value("${testing.table.name.sequence}")
    private String sequenceName;

    @Autowired
    @Qualifier("testingJdbcTemplate")
    JdbcTemplate jdbcTemplate;

    public Boolean checkExistTable() throws SQLException {
        Connection c = jdbcTemplate.getDataSource().getConnection();
        try {
            DatabaseMetaData dbm = c.getMetaData();
            ResultSet rs = dbm.getTables(null, null, tableName, null);
            return rs.next();
        } catch (Exception e) {
            log.info("Exception ", e.getMessage());
        } finally {
            c.close();
        }
        return false;
    }

    public void createTableTesting() {
        String sqlCreate = "CREATE TABLE " + tableName + " " + "(ID NUMBER(20,0), " + "CIF VARCHAR2(19 CHAR),"
                + " PROCESSING_DATE TIMESTAMP (6)," + " TOTAL_AMOUNT_IDR NUMBER(25,2)," + " YEAR_MONTH VARCHAR2(4),"
                + " CATEGORY_ID NUMBER(19,0), "
                + "PRIMARY KEY (ID) USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS " + ")";

        try {
            jdbcTemplate.execute(sqlCreate);
        } catch (Exception e) {
            log.error("Error when create table: " + e);
        }
    }

    public void createSequence() {
        try{
            String sqlCreate = "CREATE SEQUENCE " + sequenceName + tableName + " MINVALUE 1 START WITH 1 INCREMENT BY 1 CACHE 10";
            jdbcTemplate.execute(sqlCreate);

        } catch (Exception e){
            log.error("Error on sequence creation: ", e);
        }
    }

    public void executeInsertBatch(List<? extends TestingDataWriter> testingDataWriters) {
        String sqlInsert = "INSERT " + "INTO " + tableName
                + "(ID, CIF, PROCESSING_DATE, TOTAL_AMOUNT_IDR, YEAR_MONTH, CATEGORY_ID) "
                + "VALUES (?,?,?,?,?,?)";

        jdbcTemplate.batchUpdate(sqlInsert, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                NewDB newDB = testingDataWriters.get(i).getNewDB();
                ps.setLong(1, newDB.getId());
                ps.setString(2, newDB.getCif());
                ps.setObject(3, new java.sql.Date(newDB.getProcessingDate().getTime()));
                ps.setBigDecimal(4, newDB.getTotalAmountIdr());
                ps.setString(5, newDB.getYearMonth());
                ps.setInt(6, newDB.getCategoryId());

            }

            @Override
            public int getBatchSize() {
                return testingDataWriters.size();
            }
        });
    }

}
