package com.example.testing.web;

import com.example.testing.domain.BatchProcessRequest;
import com.example.testing.domain.BatchProcessResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.DuplicateJobException;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.support.ReferenceJobFactory;
import org.springframework.batch.core.launch.JobInstanceAlreadyExistsException;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping(value = "/api")
@Slf4j
public class TestingController {

    private final Job testingETLJob;
    private final JobOperator jobOperator;
    private final JobRepository jobRepository;
    private JobExecution execution;
    private final SimpleJobLauncher asyncJobLauncher;

    public TestingController(Job testingETLJob, JobOperator jobOperator, JobRepository jobRepository,
                             JobRegistry jobRegistry) throws DuplicateJobException {
        this.testingETLJob = testingETLJob;
        this.jobOperator = jobOperator;
        this.jobRepository = jobRepository;

        asyncJobLauncher = new SimpleJobLauncher();
        asyncJobLauncher.setJobRepository(jobRepository);
        asyncJobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());

        jobRegistry.register(new ReferenceJobFactory(testingETLJob));

    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/start", produces = MediaType.APPLICATION_JSON_VALUE)
    public BatchProcessResponse startPFMAssetTDBatch(@RequestBody BatchProcessRequest reqData)
            throws JobParametersInvalidException, JobInstanceAlreadyExistsException, NoSuchJobException {
        log.info("Entering controller");

        try {

//            JobParameters jobParameters = new JobParametersBuilder().addLong("schedulerId", reqData.getSchedulerId())
//                    .addDate("date", null != reqData.getDate() ? reqData.getDate() : new Date())
//                    .addDate("runDate", new Date())
//                    .addLong("runBy", Long.valueOf(reqData.getRunBy()))
//                    .addString("pfmFlag", reqData.getPfmFlag()).toJobParameters();
            JobParameters jobParameters = new JobParametersBuilder().addDate("runDate", new Date()).toJobParameters();
            asyncJobLauncher.run(testingETLJob, jobParameters);

            return new BatchProcessResponse("success", "Process Executed");

        } catch (Exception e) {
            log.error("Error", e);
            return new BatchProcessResponse("failed", e.getMessage());
        }
    }
}
