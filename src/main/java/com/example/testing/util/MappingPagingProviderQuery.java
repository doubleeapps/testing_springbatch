package com.example.testing.util;

import org.springframework.batch.item.database.support.*;
import org.springframework.batch.support.DatabaseType;
import org.springframework.jdbc.support.MetaDataAccessException;

import javax.sql.DataSource;

public class MappingPagingProviderQuery {
    public AbstractSqlPagingQueryProvider determineQueryProvider(DataSource dataSource) {
        try {
            DatabaseType databaseType = DatabaseType.fromMetaData(dataSource);
            AbstractSqlPagingQueryProvider provider;
            switch(databaseType) {
                case DERBY:
                    provider = new DerbyPagingQueryProvider();
                    break;
                case DB2:
                case DB2ZOS:
                case H2:
                    provider = new H2PagingQueryProvider();
                    break;
                case HSQL:
                    provider = new HsqlPagingQueryProvider();
                    break;
                case SQLSERVER:
                    provider = new SqlServerPagingQueryProvider();
                    break;
                case MYSQL:
                    provider = new MySqlPagingQueryProvider();
                    break;
                case ORACLE:
                    provider = new OraclePagingQueryProvider();
                    break;
                case POSTGRES:
                    provider = new PostgresPagingQueryProvider();
                    break;
                case SYBASE:
                    provider = new SybasePagingQueryProvider();
                    break;
                case SQLITE:
                    provider = new SqlitePagingQueryProvider();
                    break;
                default:
                    throw new IllegalArgumentException("Unalbe to determine PaaginQueryProvider type");
            }

            return provider;
        } catch (MetaDataAccessException e) {
            throw new IllegalArgumentException("Unable to determine PagingQueryProvider type", e);
        }
    }
}
