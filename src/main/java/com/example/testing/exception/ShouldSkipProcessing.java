package com.example.testing.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

@Slf4j
public class ShouldSkipProcessing implements SkipPolicy {
    @Override
    public boolean shouldSkip(Throwable throwable, int skipCount) throws SkipLimitExceededException {
        log.info(
                "### Skipped " + skipCount + " on " + throwable.getClass() + " with message " + throwable.getMessage());
        return true;
    }
}
