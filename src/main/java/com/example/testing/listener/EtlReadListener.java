package com.example.testing.listener;

import com.example.testing.domain.Testing;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Slf4j
@StepScope
@Component
public class EtlReadListener implements ItemReadListener<Testing> {
    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(Testing testing) {

    }

    @Override
    public void onReadError(Exception e) {

    }
}
