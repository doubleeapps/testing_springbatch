package com.example.testing.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestingCompletionProcess extends JobExecutionListenerSupport {
    public final String        TOTAL_COMMIT                                = "TOTAL_COMMIT";
    public final String        TOTAL_WRITE                                 = "TOTAL_WRITE";
    public final String        TOTAL_READ                                  = "TOTAL_READ";
    public final String        TOTAL_PROCESS                               = "TOTAL_PROCESS";
    public final String        TOTAL_SKIPPED                               = "TOTAL_SKIPPED";
    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("[PFM] Starting process listener - afterjob.....");
        log.info("TOTAL READ    {}", jobExecution.getExecutionContext().getLong(TOTAL_READ, 0));
        log.info("TOTAL WRITE   {}", jobExecution.getExecutionContext().getLong(TOTAL_WRITE, 0));
        log.info("TOTAL COMMIT  {}", jobExecution.getExecutionContext().getLong(TOTAL_COMMIT, 0));
        log.info("TOTAL SKIPPED {}", jobExecution.getExecutionContext().getLong(TOTAL_SKIPPED, 0));

        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Process Finished");
        }
    }
}
