package com.example.testing.listener;

import com.example.testing.domain.Testing;
import com.example.testing.domain.TestingDataWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Slf4j
@StepScope
@Component
public class EtlSkipListener implements SkipListener<Testing, TestingDataWriter>  {
    @Override
    public void onSkipInRead(Throwable throwable) {

    }

    @Override
    public void onSkipInWrite(TestingDataWriter testingDataWriter, Throwable throwable) {

    }

    @Override
    public void onSkipInProcess(Testing testing, Throwable throwable) {

    }
}
