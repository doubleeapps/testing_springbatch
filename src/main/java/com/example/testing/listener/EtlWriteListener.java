package com.example.testing.listener;

import com.example.testing.domain.TestingDataWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@StepScope
@Component
public class EtlWriteListener implements ItemWriteListener<TestingDataWriter> {
    @Override
    public void beforeWrite(List<? extends TestingDataWriter> list) {

    }

    @Override
    public void afterWrite(List<? extends TestingDataWriter> list) {

    }

    @Override
    public void onWriteError(Exception e, List<? extends TestingDataWriter> list) {

    }
}
