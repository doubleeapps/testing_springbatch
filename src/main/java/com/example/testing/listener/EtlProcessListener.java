package com.example.testing.listener;

import com.example.testing.domain.Testing;
import com.example.testing.domain.TestingDataWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

@Slf4j
@StepScope
@Component
public class EtlProcessListener implements ItemProcessListener<Testing, TestingDataWriter> {
    @Override
    public void beforeProcess(Testing testing) {

    }

    @Override
    public void afterProcess(Testing testing, TestingDataWriter testingDataWriter) {

    }

    @Override
    public void onProcessError(Testing testing, Exception e) {

    }
}
