package com.example.testing.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EtlStepListener extends StepExecutionListenerSupport implements StepExecutionListener {
}
