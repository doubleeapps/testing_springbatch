package com.example.testing.configuration;

import com.example.testing.decider.TestingDecider;
import com.example.testing.domain.Testing;
import com.example.testing.domain.TestingDataWriter;
import com.example.testing.exception.ShouldSkipProcessing;
import com.example.testing.listener.*;
import com.example.testing.processor.TestingProcessor;
import com.example.testing.reader.TestingReader;
import com.example.testing.tasklet.TestingTasklet;
import com.example.testing.writer.WriterTesting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Arrays;

@Configuration
@EnableBatchProcessing
@Slf4j
@EnableAsync
public class BatchConfiguration {
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${clicks.default.chunkSize}")
    private int chunkSize;

    @Autowired
    TestingProcessor testingProcessor;

    @Autowired
    WriterTesting testingWriter;

    @Autowired
    private EtlStepListener etlStepListener;

    @Autowired
    private EtlSkipListener etlSkipListener;

    @Autowired
    private EtlReadListener etlReadListener;

    @Autowired
    private EtlProcessListener etlProcessListener;

    @Autowired
    private EtlWriteListener         etlWriteListener;

    @Bean
    public CompositeItemWriter<TestingDataWriter> compositeItemWriter(WriterTesting writerTesting) {
        CompositeItemWriter<TestingDataWriter> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(Arrays.asList(writerTesting));
        return compositeItemWriter;
    }

    @Bean
    public Job testingETLJob(@Qualifier("testingStepTasklet") Step testingStepTasklet, @Qualifier("testingStep") Step testingStep, JobBuilderFactory jobBuilderFactory
    , TestingCompletionProcess testingCompletionProcess, TestingDecider testingDecider) {
        return jobBuilderFactory.get("Asset TD ETL Job")
                .incrementer(new RunIdIncrementer())
                .start(testingStepTasklet)
                .next(testingDecider)
                .on("COMPLETED")
                .to(testingStep)
                .end()
                .listener(testingCompletionProcess).build();
    }

    // Configure step
    @Bean(name = "testingStep")
    public Step testingStep(TestingReader testingReader) {
        log.info("Configure step Process...");
        return stepBuilderFactory.get("Extract -> Transform -> Aggregate -> Load")
                .<Testing, TestingDataWriter>chunk(chunkSize)
                .reader(testingReader)
                .processor(testingProcessor)
                .writer(compositeItemWriter(testingWriter))
                .listener(etlReadListener)
                .listener(etlProcessListener)
                .listener(etlWriteListener)
                .faultTolerant()
                .skipPolicy(new ShouldSkipProcessing())
                .listener(etlSkipListener)
                .listener(etlStepListener)
                .build();
    }

    @Bean(name = "testingStepTasklet")
    public Step getPfmHistoryTasklet(TestingTasklet testingTasklet) {
        return stepBuilderFactory.get("validate").tasklet(testingTasklet).build();
    }
}
