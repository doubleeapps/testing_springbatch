package com.example.testing.configuration;

import com.example.testing.tasklet.TestingTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {
    @Primary
    @Bean(name = "testingDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.testing")
    public DataSource testingDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dwhDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.dwh")
    public DataSource dwhDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "testingJdbcTemplate")
    public JdbcTemplate testingJdbcTemplate() {
        return new JdbcTemplate(testingDataSource());
    }
}
