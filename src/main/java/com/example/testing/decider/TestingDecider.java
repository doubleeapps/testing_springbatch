package com.example.testing.decider;

import com.example.testing.repository.NewDBRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Slf4j
@Component
@EnableBatchProcessing
public class TestingDecider implements JobExecutionDecider {

    @Autowired
    NewDBRepository newDBRepository;

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        try {
            if (!newDBRepository.checkExistTable()) {
                newDBRepository.createSequence();
                newDBRepository.createTableTesting();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return FlowExecutionStatus.COMPLETED;
    }
}
