package com.example.testing.domain;

import lombok.Data;

import java.util.Date;


@Data
public class BatchProcessRequest {
    private int    runBy;
    private Long   schedulerId;
    private String pfmFlag;
    private Date   date;
}
