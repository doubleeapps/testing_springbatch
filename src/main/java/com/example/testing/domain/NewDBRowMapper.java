package com.example.testing.domain;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewDBRowMapper implements RowMapper<NewDB> {
    @Override
    public NewDB mapRow(ResultSet rs, int i) throws SQLException {
        return NewDB.builder()
                .id(rs.getLong("ID"))
                .cif(rs.getString("CIF"))
                .processingDate(rs.getDate("PROCESSING_DATE"))
                .totalAmountIdr(rs.getBigDecimal("TOTAL_AMOUNT_IDR"))
                .yearMonth(rs.getString("YEAR_MONTH"))
                .categoryId(rs.getInt("CATEGORY_ID"))
                .build();
    }
}
