package com.example.testing.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BatchProcessResponse {
    private String status;
    private String message;
}
