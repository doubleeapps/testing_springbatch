package com.example.testing.domain;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TestingRowMapper implements RowMapper<Testing> {
    public Testing mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Testing.builder().businessDate(rs.getDate("BUSINESS_DATE")).id(rs.getLong("ID"))
                .cbdate(rs.getInt("CBDATE")).cbcifno(rs.getString("CBCIFNO")).cbactn(rs.getLong("CBACTN"))
                .cbsccd(rs.getString("CBSCCD")).cbctyp(rs.getString("CBCTYP")).cbscds(rs.getString("CBSCDS"))
                .cbsctc(rs.getString("CBSCTC")).cbsctm(rs.getString("CBSCTM")).cblbal(rs.getBigDecimal("CBLBAL"))
                .cblbale(rs.getBigDecimal("CBLBALE")).cbbkcd(rs.getString("CBBKCD")).cbfxrt(rs.getFloat("CBFXRT")).build();
    }
}
