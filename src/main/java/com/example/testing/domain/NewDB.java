package com.example.testing.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewDB {
    private Long       id;
    private String     cif;
    private Date processingDate;
    private BigDecimal totalAmountIdr;
    private String     yearMonth;
    private int        categoryId;
}
