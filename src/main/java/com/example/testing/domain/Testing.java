package com.example.testing.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Testing {
    private Date businessDate;
    private Long       id;
    private int        cbdate;
    // cif number
    private String     cbcifno;
    // account number
    private Long       cbactn;
    // product type
    private String     cbsccd;
    // currency
    private String     cbctyp;
    // product type description
    private String     cbscds;
    // product type term code
    private String     cbsctc;
    // product type term
    private String     cbsctm;
    // ledger balance
    private BigDecimal cblbal;
    // ledger balance in IDR
    private BigDecimal cblbale;
    // flag convent sharia
    private String     cbbkcd;
    // exchange rate
    private Float      cbfxrt;
}
